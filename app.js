const express = require('express');
const mongoose = require('mongoose')
const bodyParser = require('body-parser');
const methodOverride = require('method-override');

mongoose.connect('mongodb+srv://admin:ElPjgmWZ4kxLWFC1@cluster0-z1t8q.gcp.mongodb.net/test?retryWrites=true',{ useNewUrlParser: true }).then(() => {
    console.log('Connexion à la bdd : OK!')
})
const app = express();

app.use(bodyParser.urlencoded({extended : false}));
app.use(methodOverride('_method'));
app.set('view engine','pug');

const indexRoutes = require('./routes/index');
app.use('/', indexRoutes);
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Serveur lancé sur le port ${port}.`))