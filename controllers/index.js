const Todo = require('../models/Todo');

// Récupération
const getIndex = (req,res) => {
    Todo.find({}, (err,todos) => {
        if(err){
            console.log(err);
        }
        res.render('index',{
            todos: todos
        });
    });
}

const postIndex = (req,res) => {
    const newTodo = new Todo({
       titre: req.body.titre
    });

    newTodo.save((err) => {
        if(err){
            console.log(err);
        } else {
            res.redirect('/');
        }
    });
}

const deleteIndex = (req,res) => {
    const {id} = req.params;
    Todo.findByIdAndDelete(id, (err) => {
        if(err){
            console.log(err);
        } else {
            res.redirect('/');
        }
    });
}

module.exports = {
    getIndex: getIndex,
    postIndex: postIndex,
    deleteIndex: deleteIndex
}