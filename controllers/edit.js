const Todo = require('../models/Todo');
const getEdit = (req,res) => {
    const {id} = req.params;
    Todo.findById(id, (err,todo) => {
        if(err){
            console.log(err)
        } else {
            res.render('edit', {
                todo: todo
            });
        }

    });
}

const putEdit = (req,res) => {
    const {id} = req.params;
    Todo.findOneAndUpdate(id,{titre: req.body.newtitre }, err => {
        if(err){
            console.log(err);
        } else {
            res.redirect('/');
        }
    });
}

module.exports = {
    getEdit : getEdit,
    putEdit : putEdit
}